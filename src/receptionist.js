// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.

// <-start-
class Receptionist {
  constructor () {
    this.list = [];
    this.listForId = [];
  }

  register (a) {
    if (a === null || a === undefined || a === '' || Object.keys(a).length === 0 ||
    !Object.prototype.hasOwnProperty.call(a, 'id') || !Object.prototype.hasOwnProperty.call(a, 'name')) {
      throw new Error('Invalid person');
    } else if (this.listForId.includes(a.id) === true) {
      throw new Error('Person already exist');
    } else if (a.id !== null && a.name !== null) {
      this.list.push(a);
      this.listForId.push(a.id);
    }
    return this.list;
  }

  getPerson (num) {
    if (this.listForId.includes(num) === true) {
      for (let i = 0; i < this.list.length; i++) {
        if (this.list[i].id === num) {
          return this.list[i];
        }
      }
    } else {
      throw new Error('Person not found');
    }
  }
}

// --end-->

export default Receptionist;
