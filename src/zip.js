// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (array1, array2) {
  if (array1.length === 0 && Array.isArray(array1)) {
    return array1;
  } else if (array2.length === 0 && Array.isArray(array2)) {
    return array2;
  }

  var list = [];
  if (array1.length <= array2.length) {
    for (let i = 0; i < array1.length; i++) {
      list.push([array1[i], array2[i]]);
    }
    return list;
  }
  if (array1.length > array2.length) {
    for (let i = 0; i < array2.length; i++) {
      list.push([array1[i], array2[i]]);
    }
    return list;
  }
}
// --end->

export default zip;
